## Mariadb

### How to use this image?

`docker run --detach --name mariadb --env MARIADB_USER=user --env MARIADB_PASSWORD=my_cool_secret --env MARIADB_ROOT_PASSWORD=my-secret-pw  ghcr.io/dockenv/mariadb:latest`
